#### Compatible with tensorflow v1.10 ########

import pandas as pd
import numpy as np
import tensorflow as tf

import keras
from keras import backend as K
from keras import layers, models, optimizers, regularizers, callbacks
from keras_preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split

# configure Tensorflow GPU options
config = tf.compat.v1.ConfigProto( gpu_options = tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.8) )  # device_count = {'GPU': 1}
config.gpu_options.allow_growth = True
session = tf.compat.v1.Session(config=config)
K.set_session(session)

# decorator for multi-output layer generators
class MultiOutputGenerator:
    def __init__(self, gen):
        self._gen = gen
    
    def __iter__(self):
        return self
    
    def __next__(self):
        ret = self._gen.next()
        x, y = ret
        y = [y_col for y_col in y.T] if y.ndim > 1 else [y]
        return x, y
    
    @property
    def n(self):
        return self._gen.n
    
    @property
    def batch_size(self):
        return self._gen.batch_size
  
    
class CNNmodel:
    def __init__(self):
        self._model = None
        
    # build CNN model
    def build_model(self, 
                    num_conv_layers=4,
                    num_conv_filters=32,
                    num_dense_layers=1,
                    num_dense_units=32,
                    activation='relu', 
                    batch_norm=False,
                    dropout=0.0,
                    kernel_l1=0.0, 
                    kernel_l2=0.0,
                    batch_size=16,
                    target_names=['pose_1' 'pose_3'], 
                    size=(128,128),
                    **kwargs):
        K.clear_session()
    
        input_dims = (size[0], size[1], 1)
        output_dim = len(target_names)
        num_hidden_layers = num_conv_layers + num_dense_layers
    
        # convolutional layers
        inp = keras.Input(shape=input_dims, name='input')
        x = inp
        for i in range(num_conv_layers):
            x = layers.Conv2D(filters=num_conv_filters, kernel_size=3)(x)
            if batch_norm:
                x = layers.BatchNormalization()(x)
            x = layers.Activation(activation)(x)
            x = layers.MaxPooling2D()(x)
    
        # dense layers
        x = layers.Flatten()(x)
        for i in range(num_conv_layers, num_hidden_layers):
            if dropout > 0:
                x = layers.Dropout(dropout)(x)
            x = layers.Dense(units=num_dense_units,
                kernel_regularizer=regularizers.l1_l2(l1=kernel_l1, l2=kernel_l2))(x)         
            x = layers.Activation(activation)(x)            
    
        # Output layers
        if dropout > 0:
            x = layers.Dropout(dropout)(x)
        out = [layers.Dense(units=1,
                    name=('output_' + str(i + 1)),
                    kernel_regularizer=regularizers.l1_l2(l1=kernel_l1, l2=kernel_l2))(x)
                    for i in range(output_dim)]
    
        self._model = models.Model(inp, out)
    
    def compile_model(self, 
                      output_dim = None,
                      lr = 1e-4,
                      decay = 1e-6,
                      loss_weights = None,
                      **kwargs):
        if output_dim==None:
            output_dim = len(loss_weights)
        self._model.compile(optimizer=optimizers.Adam(lr=lr, decay=decay),
                            loss=['mse',] * output_dim,
                            loss_weights=loss_weights,
                            metrics=['mae']) 
    
    def fit_model(self, 
                  train_df_file,
                  train_image_dir, 
                  target_names, 
                  size, 
                  batch_size, 
                  epochs,
                  valid_df_file = None,
                  valid_image_dir = None, 
                  valid_split = 0.0,
                  patience = 10,
                  model_file = 'model.h5',
                  verbose = 0,
                  **kwargs):      
        
        target_df = pd.read_csv(train_df_file)
        
        if valid_image_dir is not None and valid_df_file is not None:
            train_df = target_df
            valid_df = pd.read_csv(valid_df_file)
        else:
            train_df, valid_df = train_test_split(target_df, test_size=valid_split)  
            valid_image_dir = train_image_dir
            
        train_datagen = ImageDataGenerator(rescale=1./255.)   
        train_generator = train_datagen.flow_from_dataframe(
                                dataframe=train_df,
                                directory=train_image_dir,
                                x_col='image_name',
                                y_col=target_names,
                                batch_size=batch_size,
                                seed=42,
                                shuffle=True,
                                class_mode='other',
                                target_size=size,
                                color_mode='grayscale')
        
        valid_datagen = ImageDataGenerator(rescale=1./255.)    
        valid_generator = valid_datagen.flow_from_dataframe(
                                dataframe=valid_df,
                                directory=valid_image_dir,
                                x_col='image_name',
                                y_col=target_names,
                                batch_size=batch_size,
                                seed=42,
                                shuffle=True,
                                class_mode='other',
                                target_size=size,
                                color_mode='grayscale')        
        
        train_callbacks = [
            callbacks.EarlyStopping(monitor='val_loss', patience=patience, verbose=1),
            callbacks.ModelCheckpoint(filepath=model_file, monitor='val_loss', save_best_only=True)]                  
        
        history =  self._model.fit_generator(
                      generator=MultiOutputGenerator(train_generator),
                      steps_per_epoch=train_generator.n // train_generator.batch_size,
                      epochs=epochs,
                      verbose=verbose,
                      callbacks=train_callbacks,           
                      validation_data=MultiOutputGenerator(valid_generator),
                      validation_steps=valid_generator.n // valid_generator.batch_size)
        return history.history
    
    def predict(self, image):       
        pred = self._model.predict(x=image, verbose=1)
        pred = np.array([np.squeeze(pred_array) for pred_array in pred]) 
        return pred.T
    
    def predict_from_file(self, test_image_dir, 
                          test_df_file,
                          target_names,
                          batch_size,
                          size, 
                          **kwargs):
    
        test_datagen = ImageDataGenerator(rescale=1./255.)
        test_generator = test_datagen.flow_from_dataframe(
                            dataframe=pd.read_csv(test_df_file),
                            directory=test_image_dir,
                            x_col='image_name',
                            y_col=target_names,
                            batch_size=batch_size,
                            seed=42,
                            shuffle=False,
                            class_mode='other',
                            target_size=size,
                            color_mode='grayscale')
            
        pred = self._model.predict_generator(generator=test_generator, verbose=1)    
        pred = np.array([np.squeeze(pred_array) for pred_array in pred])
        return pred.T
         
    def save_model(self, model_file, **kwargs):
        self._model.save(model_file)
            
    def load_model(self, model_file, **kwargs):
        self._model =  models.load_model(model_file)
    
    def print_model_summary(self):
        self._model.summary()
    

def main():
    pass
    
if __name__ == '__main__':
    main() 
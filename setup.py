"""Setup file for Pose Models for 3D Surfaces and Edges
"""

from setuptools import setup

base_deps = ["numpy","pandas","matplotlib"]

optimization_deps = ["hyperopt"]
deep_learning_deps = ["tensorflow==1.10"]
deep_learning_gpu_deps = ["tensorflow-gpu==1.10"]

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
    name="pose-models-3d",
    version="1.0",
    description="Pose Models for 3D Surfaces and Edges",
    license="GPLv3",
    long_description=long_description,
    author="Nathan Lepora",
    author_email="n.lepora@bristol.ac.uk",
    url="https://github.com/nlepora/",
    packages=["pose_models_3d"],
    install_requires=[base_deps],
    extras_require={
        "optimization": optimization_deps,
        "deep_learning": deep_learning_deps,
        "deep_learning_gpu": deep_learning_gpu_deps,
    },
)

# Tactile Pose Models for 3D Surfaces and Edges 

A Python library for collecting optical tactile data for training pose models using convolutional neural networks on the tactile images.

Methods described in  
Optimal Deep Learning for Robot Touch: Training Accurate Pose Models of 3d Surfaces and Edges  
N Lepora, J Lloyd (2020) IEEE Robotics & Automation Magazine  
https://arxiv.org/pdf/2003.01916.pdf

Data and models use (x, y, z, rx, ry, rz) components of pose, suitable for >=6DoF robot arms. Here an ABB IRB120 is used.

## Installation

To install the package on Windows or Linux, clone the repository and run the setup script from the repository root directory:

```sh
pip install -e .
```

Code was developed and run from Visual Studio Code.

## Examples

Some examples that demonstrate how to use the library are included in the `\examples` directory.  

- For an ABB IRB120 robot
	```sh
	python abb_robot_test.py
	```

## Requirements

Needs installation of these packages

Common Robot Interface (CRI) fork at  
https://github.com/nlepora/cri

Video Stream Processor (VSP) fork at  
https://github.com/nlepora/vsp 

Also needs Tensorflow 1 and Keras; tested with  
tensorflow==1.14.0  
tensorflow-gpu=1.14.0  
Keras==2.3.1

Some functionality also needs hyperopt; tested with  
hyperopt==0.2.5

## Workflow

0. Check the installation with \examples
1. Collect the tactile train and test data for 2d edge, 3d edge or 3d surface with scripts in \collect
2. Process training data into train and validation sets; optional processing of test data 
3. Train the pose models with scripts in \train: (a) single model; (b) optimize model hyperparameters
4. Test the pose models with scripts in \test: (a) single model; (b) batch of models from optimizer

Note: all code requires an environment variable DATAPATH to the data directory.

## Precollected data and models

Data and models from using this code are available in the data repository
tactile-servoing-3d-abb

Available for  
- tactip-127: Hemispherical TacTip (127 pin version; 40mm dia.)

Has 3 types of pose model/data  
- model_edge2d: 2d pose of a horizontal edge (x, rz)  
https://bit.ly/3vEK8rv  
- model_surface2d: 3d pose of a surface (z, rx, ry)   
https://bit.ly/3kbfIIg   
- model_edge3d: 3d pose of an edge (x, z, rx, ry, rz)  
https://bit.ly/3kcfoZQ

Links available to University of Bristol accounts.

## Papers

The methods, data and models are described and used in this paper

Optimal Deep Learning for Robot Touch: Training Accurate Pose Models of 3d Surfaces and Edges  
N Lepora, J Lloyd (2020) IEEE Robotics & Automation Magazine  
https://arxiv.org/pdf/2003.01916.pdf

Related papers 

Pose-Based Tactile Servoing: Controlled Soft Touch with Deep Learning  
N Lepora, J Lloyd (2021) IEEE Robotics & Automation Magazine  
https://arxiv.org/pdf/2012.02504.pdf 

From Pixels to Percepts: Highly Robust Edge Perception and Contour Following using Deep Learning and an Optical Biomimetic Tactile Sensor  
N Lepora et al (2019) IEEE Robotics & Automation Letters  
https://arxiv.org/pdf/1812.02941.pdf 

## Meta

pose-models-3d:

Nathan Lepora – n.lepora@bristol.ac.uk

[https://bitbucket.org/nlepora/pose-models-3d](https://bitbucket.org/nlepora/pose-models-3d)

Distributed under the GPL v3 license. See ``LICENSE`` for more information.
